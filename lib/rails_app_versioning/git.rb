module RailsAppVersioning
  module Git
    def git_current_branch
      git_command("git branch").split(/\n/).grep(/\*/).first.gsub('* ', '')
    end

    def git_checkout(branch = 'master')
      git_command("git checkout #{branch}")
    end

    def git_pull(branch = 'master')
      git_command("git pull origin #{branch}")
    end

    def git_commit(message = nil)
      git_command("git add VERSION")
      git_command("git commit --ammend")
    end

    def git_push(branch = 'master')
      git_command("git push origin #{branch}")
    end

    def git_command(str)
      systemu(str).drop(1).join(" ")
    end
  end
end
